﻿# NeoPixel Swarm LED Demo
##Introduction
Inspired by Stefan Scherer's (@stefscherer) similar setup, and looking for a fun demo around the office, a mini swarm seemed like a good idea. With the internet out of Blinkt! LED's we turned to Adafruit NeoPixels. Which in turn necessitated python to use the library, which meant reinventing the wheel. 

##Features
This project went a little further than some I have seen, dynamically swarming and assigning colors to any containers used.