#Add to device-init.yaml
#runcmd:
#   - "bash /boot/BuildWorker.sh" 
#   - "python /home/pirate/dockermanager.py"


#install packages
apt-get update
apt-get install build-essential python-dev git scons swig arping redis-server redis-tools libnss-mdns

#get rpi NeoPixel Library
git clone https://github.com/jgarff/rpi_ws281x.git
cd rpi_ws281x
scons
cd python
python setup.py install

cd ~
wget https://bootstrap.pypa.io/get-pip.py
python get-pip.py
pip install redis docker-py
git clone https://bitbucket.org/MattSollie/rpi-neopixel-swarm.git
cd rpi-neopixel-swarm