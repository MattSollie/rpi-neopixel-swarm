#install packages
apt-get update
apt-get install build-essential python-dev git scons swig arping redis-server redis-tools libnss-mdns

#get rpi NeoPixel Library
git clone https://github.com/jgarff/rpi_ws281x.git
cd rpi_ws281x
scons
cd python
sudo python setup.py install

cd ~
wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
sudo pip install redis
pip install docker-py --update
git clone https://bitbucket.org/MattSollie/rpi-neopixel-swarm.git
cd rpi-neopixel-swarm

