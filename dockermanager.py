import random
import time
import redis
import requests
import socket
import docker
from neopixel import *

# LED strip configuration:
LED_COUNT      = 8      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 50     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor$
colors = [[108,0,127],[100,127,0],[0,127,0],[0,0,127],[127,127,0],[127,0,0]] #Container LED Color Pool, decided by manager

#Chase pattern used during disconnects
def theaterChase(strip, ccolor, wait_ms=200, iterations=5):
        for j in range(iterations):
                for q in range(3):
                        for i in range(0, strip.numPixels(), 3):
                                strip.setPixelColor(i+q, ccolor)
                        strip.show()
                        time.sleep(wait_ms/1000.0)
                        for i in range(0, strip.numPixels(), 3):
                                strip.setPixelColor(i+q, 0)
def getMyIP():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]
    s.close()

myip = getMyIP()
redisConnected = False
while redisConnected == False:
    try:
        r = redis.StrictRedis(host='localhost', port=6379, db=0) #Local redis connection        
        redisConnected = True
        r.set('swarmaddr',myip)
        
    except:
        theaterChase(strip, Color(50,10, 0))

#Default NeoPixel itialialization
strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS)
strip.begin()

#Connect to Docker Socket
dockerConnected = False
while dockerConnected == False:
    try:
        client = docker.from_env()
        #test connection
        workertoken = client.swarm.attrs['JoinTokens']['Worker']
        r.set("swarmworkertoken",workertoken)
        dockerConnected = True
    except:
        theaterChase(strip, Color(50,10, 0))


#Destination dict for running docker confit
images = {}

#update all pixels with data in images dict
def UpdatePixels(images):
    try:
        i = 0
        for container, cdata in images.iteritems():
            if cdata['count'] > 0:
                for n in range(cdata['count']):
                    strip.setPixelColor(i, Color(cdata['color'][0],cdata['color'][1],cdata['color'][2]))
                    i += 1
        # Set blank pixels off
        for n in range(i,LED_COUNT):
            strip.setPixelColor(i, Color(0,0,0))
            i += 1
        strip.show()
    except:
        print("Error Updating Pixels")
        return



#Create redis entries for all existing images for worker nodes to work off
#Store by tag and Digest to support services or individual containers
def CheckImages(client=client, r=r, colors=colors, images=images):
    docker_images = client.images.list()
    for image in docker_images:
            color = colors.pop()
            if image.attrs['RepoDigests']:
                for digest in image.attrs['RepoDigests']:
                    r.delete(digest)
                    r.rpush(digest,color[0])
                    r.rpush(digest,color[1])
                    r.rpush(digest,color[2])
                    images[digest] = {'color': color, 'count': 0}
            if image.attrs['RepoTags']:
                for tag in image.attrs['RepoTags']:
                    r.delete(tag)
                    r.rpush(tag,color[0])
                    r.rpush(tag,color[1])
                    r.rpush(tag,color[2])
                    images[tag] = {'color': color, 'count': 0}   
            

#Add existing containers from docker to running config, in case they were running at the script startup
def CheckRunningContainers(client=client):
    running =  client.containers.list()
    for container in running:
        images[str(container.attrs['Config']['Image'])] = {'color': images[str(container.attrs['Config']['Image'])]['color'], 'count': 0}
    for container in running:
        images[str(container.attrs['Config']['Image'])]['count'] +=1
    UpdatePixels(images)

#### Start Work ###
print("Checking Images")
CheckImages()

#Run Main Watcher Loop
while True:
    try: 
        CheckRunningContainers()
        print("listening for events")        
        for event in events:
            if event['Type'] == 'container':
                if event['status'] == 'start':
                        print("container started: " + event['from'])
                        if r.exists(event['from']):
                                print("  existing image")
                                color = r.lrange(event['from'],0,-1)

                        else:
                                print("  new image")
                                color = colors.pop()
                                r.rpush(event['from'],color[0])
                                r.rpush(event['from'],color[1])
                                r.rpush(event['from'],color[2])
                                images[event['from']] =  {'color': color, 'count': 0}
                        images[event['from']]['count'] += 1
                elif event['status'] == 'stop':
                        print("container stopped: " + event['from'])
                        if images[event['from']]['count'] > 0:
                            images[event['from']]['count'] -= 1
                        
                UpdatePixels(images)
    except:
        #Docker disconnected
        print("Event Stream Broken")
        
    
    try:
        #Attempt Reconnect
        events = client.events(decode=True)
    except:
        #display disconnect LEDS
        print("waiting for docker")
        theaterChase(strip, Color(5,50, 0))

print('exited')
