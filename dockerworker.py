import random
import time
import redis
import requests
import base64
import docker
from neopixel import *

# LED strip configuration:
LED_COUNT      = 8      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 100     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor$
master_name = "black-pearl.local"


#if docker info -f '{{json .Swarm.LocalNodeState}}'
#Get token from redis
#redis-cli set swarmworkertoken $TOKEN

#Chase pattern used during disconnects
def theaterChase(strip, ccolor, wait_ms=200, iterations=5):
        for j in range(iterations):
                for q in range(3):
                        for i in range(0, strip.numPixels(), 3):
                                strip.setPixelColor(i+q, ccolor)
                        strip.show()
                        time.sleep(wait_ms/1000.0)
                        for i in range(0, strip.numPixels(), 3):
                                strip.setPixelColor(i+q, 0)
                                
redisConnected = False
while redisConnected == False:
    try:
        r = redis.StrictRedis(host=master_name, port=6379, db=0) #Local redis connection
        masterip = r.get("swarmaddr")
        print("Master Discovered: " + masterip)
        redisConnected = True
    except:
        theaterChase(strip, Color(50,10, 0))

#Default NeoPixel itialialization
strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS)
strip.begin()

#Connect to Docker Socket
dockerConnected = False
while dockerConnected == False:
    try:
        client = docker.from_env()
        #test connection
        current_swarmmanager = client.info()['Swarm']['RemoteManagers'][0]['Addr']
        print("Current Swarm Manager: " +  current_swarmmanager)
        events = client.events(decode=True)
        dockerConnected = True
    except:
        theaterChase(strip, Color(50,10, 0))


#Destination dict for running docker confit
images = {}

#update all pixels with data in images dict
def UpdatePixels(images):
    try:
        i = 0
        for container, cdata in images.iteritems():            
            if cdata['count'] > 0:
                for n in range(cdata['count']):
                    strip.setPixelColor(i, Color(int(cdata['color'][0]),int(cdata['color'][1]),int(cdata['color'][2]) ) ) 
                    i += 1
        # Set blank pixels off
        for n in range(i,LED_COUNT):
            strip.setPixelColor(i, Color(0,0,0))
            i += 1
        strip.show()
    except Exception, e:
        print("Error Updating Pixels" + str(e))
        return


#Add existing containers from docker to running config, in case they were running at the script startup
def CheckRunningContainers(client=client):
    running =  client.containers.list()
    for container in running:
        c = r.lrange(container.attrs['Config']['Image'],0,2)
        images[container.attrs['Config']['Image']] = {'color': c, 'count': 0}
    for container in running:
        images[container.attrs['Config']['Image']]['count'] +=1
    UpdatePixels(images)

#### Start Work ##

#Run Main Watcher Loop
while True:
    try:        
        CheckRunningContainers()        
        for event in events:
            if event['Type'] == 'container':
                if event['status'] == 'start':
                        print("container started: " + event['from'])                        
                        if r.exists(event['from']):
                            if images[event['from']]:
                                print("  existing image")
                                color = r.lrange(event['from'],0,2)
                            else: 
                                print("  existing image in swarm. adding locally")
                                color = r.lrange(event['from'],0,2)
                                images[container['Image']] = {'color': color, 'count': 0}
                        else:
                                print("  missing image in redis")
                                color = [108,0,127]
                                #color = colors.pop()
                                #r.rpush(event['from'],color[0])
                                #r.rpush(event['from'],color[1])
                                #r.rpush(event['from'],color[2])
                                #images[event['from']] =  {'color': color, 'count': 0}
                        images[event['from']]['count'] += 1
                elif event['status'] == 'stop':
                        print("container stopped: " + event['from'])
                        if images[event['from']]['count'] > 0:
                            images[event['from']]['count'] -= 1
                        
                UpdatePixels(images)
    except Exception, e:
        #Docker disconnected
        print("Event Stream Broken:" + str(e))
        
    
    try:
        #Attempt Reconnect
        CheckRunningContainers()        
        events = client.events(decode=True)        
    except:
        #display disconnect LEDS
        print("waiting for docker")
        theaterChase(strip, Color(5,50, 0))

print('exited')
